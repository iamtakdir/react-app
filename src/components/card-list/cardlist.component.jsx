import "./card-list.css";
import Card from "../Card/card.component";

import React from "react";

const CardList = ({ monsters }) => (
  <div className="card-list">
    {monsters.map((monster) => {
      return <Card key={monster.id} monster={monster} />;
    })}
  </div>
);
export default CardList;
