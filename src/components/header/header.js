import { useState } from "react";
import "./header.css";

import React from "react";

function Header(props) {
  const [title, setTitle] = useState("Welcome To monster Bash");
  return (
    <center>
      <div className="Header">
        <h1>{title.toUpperCase()}</h1>
      </div>
    </center>
  );
}

export default Header;
